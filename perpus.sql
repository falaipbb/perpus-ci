-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Apr 2020 pada 06.51
-- Versi server: 10.1.28-MariaDB
-- Versi PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(5) NOT NULL,
  `nama_admin` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `username`, `password`) VALUES
(3, 'Fauzi', 'pay', '202cb962ac59075b964b07152d234b70'),
(4, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(5, 'pay', 'pay', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `nama_anggota` varchar(45) NOT NULL,
  `gender` enum('laki-laki','perempuan') NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `username`, `nama_anggota`, `gender`, `no_telp`, `alamat`, `email`, `password`) VALUES
(4, '', 'Nur', 'laki-laki', '0812121213', 'Bekasi', 'falaipbb@gmail.com', '202cb962ac59075b964b07152d234b70'),
(5, 'trash', 'Achmad', 'laki-laki', '0812121214', 'Bekasi', 'falaipbb@gmail.com', '202cb962ac59075b964b07152d234b70'),
(6, 'pay', 'pay', 'laki-laki', '08121212', 'Bekasi', 'falaipbb@gmail.com', '202cb962ac59075b964b07152d234b70'),
(7, '', 'Achmadd', 'laki-laki', '08121212', 'Bekasi', 'falaipbb@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(8, '', 'FauziPayy', 'laki-laki', '08121212', 'bekasi', 'falaipbb@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, '', 'Fauzi', 'laki-laki', '08121212', 'Bekasi', 'falaipbb@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(2) NOT NULL,
  `id_kategori` int(2) NOT NULL,
  `judul_buku` varchar(50) NOT NULL,
  `pengarang` varchar(35) NOT NULL,
  `thn_terbit` date NOT NULL,
  `penerbit` varchar(25) NOT NULL,
  `isbn` varchar(25) NOT NULL,
  `jumlah_buku` int(3) NOT NULL,
  `lokasi` varchar(20) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `tgl_input` date NOT NULL,
  `status_buku` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `id_kategori`, `judul_buku`, `pengarang`, `thn_terbit`, `penerbit`, `isbn`, `jumlah_buku`, `lokasi`, `gambar`, `tgl_input`, `status_buku`) VALUES
(12, 4, 'Kisah 7 Bayi Bisa Bicara', 'Uwais Ramadhan, Lc', '2015-02-04', 'Gema Insani', '978', 86, 'Bekasi', 'gambar1555380111.jpg', '2019-03-29', '1'),
(15, 5, 'Pemrograman Database dengan Delphi7', 'Abdul Kadir', '2017-10-02', 'Andi', '519.6.KAD.P.3', 133, 'Jakarta', 'gambar1555379748.jpg', '2019-04-16', '1'),
(16, 5, 'Sistem Operasi Microsoft Windows XP Professional', 'Ian Chandra K', '2016-04-27', 'Elex Media Komputindo', '519.8.CHA.S.1', 185, 'Jakarta', 'gambar1555379930.jpeg', '2019-04-16', '1'),
(17, 5, 'Algoritma & Teknik Pemrograman', 'Budi Sutejo, Michael An', '2010-09-09', 'ANDI', '519.6.SUT.A.1', 93, 'Jakarta', 'gambar1555380082.jpeg', '2019-04-16', '1'),
(18, 5, 'Manajemen Database dengan Microsoft Visual Basic', 'M Agus, J. Alam', '2016-04-29', 'Elex Media Komputindo', '519.8.AGU.M.1', 149, 'Jakarta', 'gambar1555899144.jpeg', '2019-04-22', '1'),
(19, 4, 'Hadits Dan Kisah Teladan Muslim', 'Ahmad Saifudin', '2019-04-03', 'Noktah', '978-602-250-235-5', 199, 'Bekasi', 'gambar1555899527.jpg', '2019-04-22', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_pinjam` varchar(10) NOT NULL,
  `id_buku` int(5) NOT NULL,
  `denda` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_pinjam`, `id_buku`, `denda`) VALUES
('PJ001', 16, 10000),
('PJ001', 17, 10000),
('PJ003', 12, 10000),
('PJ003', 15, 10000),
('PJ003', 16, 10000),
('PJ003', 17, 10000),
('PJ003', 18, 10000),
('PJ003', 19, 10000),
('PJ004', 15, 10000),
('PJ005', 15, 10000),
('PJ006', 12, 10000),
('PJ007', 15, 10000),
('PJ008', 16, 10000),
('PJ001', 16, 10000),
('PJ001', 17, 10000),
('PJ001', 15, 10000),
('PJ001', 16, 10000),
('PJ002', 15, 10000),
('PJ002', 16, 10000),
('PJ003', 15, 10000),
('PJ003', 16, 10000),
('PJ004', 12, 10000),
('PJ004', 17, 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Comedy'),
(2, 'Action'),
(3, 'Sport'),
(4, 'Islam'),
(5, 'Komputer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_pinjam` varchar(5) NOT NULL,
  `tanggal_input` datetime NOT NULL,
  `id_anggota` int(5) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `tgl_pengembalian` date NOT NULL,
  `totaldenda` double NOT NULL,
  `status_peminjaman` enum('Booking','Selesai','Belum Selesai') NOT NULL,
  `status_pengembalian` enum('kembali','belum kembali') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_pinjam`, `tanggal_input`, `id_anggota`, `tgl_pinjam`, `tgl_kembali`, `tgl_pengembalian`, `totaldenda`, `status_peminjaman`, `status_pengembalian`) VALUES
('PJ001', '2019-05-08 07:05:40', 8, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Booking', 'belum kembali'),
('PJ002', '2019-05-18 17:05:26', 8, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Booking', 'belum kembali'),
('PJ003', '2019-05-20 09:05:46', 8, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Booking', 'belum kembali'),
('PJ004', '2020-02-03 05:02:01', 4, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Booking', 'belum kembali');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_pinjam` varchar(5) NOT NULL,
  `tgl_pencatatan` datetime NOT NULL,
  `id_anggota` int(4) NOT NULL,
  `id_buku` int(4) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `denda` double NOT NULL,
  `tgl_pengembalian` date NOT NULL,
  `total_denda` double NOT NULL,
  `status_pengembalian` varchar(15) NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_pinjam`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
