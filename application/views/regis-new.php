<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login - Perpustakaan Online</title>
    <script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
    <style type="text/css">
        h2 {
            font-family: Stencil;
            font-size: 35px;
            color: wheat;
        }

        .wrap {
            background-size: cover;
        }

        a {
            color: #58bff6;
            text-decoration: none;
            padding: 6px;
        }

        a:hover {
            color: #aaa;
        }

        .pull-right {
            float: right;
        }

        .pull-left {
            float: left;
        }

        .clear-fix {
            clear: both;
        }

        div.logo {
            text-align: center;
            margin: 20px 20px 30px 20px;
            fill: #566375;
        }

        #formWrapper {
            background: rgba(0, 0, 0, .2);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            transition: all .3s ease;
        }

        .darken-bg {
            background: rgba(0, 0, 0, .5) !important;
            transition: all .3s ease;
        }

        div#form {
            position: absolute;
            width: 498px;
            height: 320px;
            height: auto;
            background-color: transparent;
            margin: auto;
            border-radius: 5px;
            padding: 20px;
            left: 45%;
            top: 45%;
            margin-left: -180px;
            margin-top: -200px;
        }

        div.form-item {
            position: relative;
            display: block;
            margin-bottom: 20px;
        }

        input {
            transition: all .2s ease;
        }

        input.form-style {
            display: block;
            width: 90%;
            height: 25px;
            padding: 5px 5%;
            border: 1px solid #ccc;
            border-radius: 27px;
            background-clip: padding-box;
            background-color: #fff;
            font-family: 'HelveticaNeue', 'Arial', sans-serif;
            letter-spacing: .8px;
        }

        div.form-item .form-style:focus {
            outline: none;
            border: 1px solid #58bff6;
        }

        div.form-item p.formLabel {
            position: absolute;
            left: 26px;
            top: 2px;
            transition: all .4s ease;
            color: #bbb;
        }

        .formTop {
            top: -22px !important;
            left: 26px;
            background-color: #fff;
            padding: 0 5px;
            font-size: 14px;
        }

        .formStatus {
            color: #8a8a8a !important;
        }

        input[type="submit"].login {
            float: right;
            width: 112px;
            height: 37px;
            -moz-border-radius: 19px;
            -webkit-border-radius: 19px;
            border-radius: 19px;
            -moz-background-clip: padding;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            background-color: #55b1df;
            border: 1px solid #55b1df;
            border: none;
            color: #fff;
            font-weight: bold;
        }

        input[type="submit"].login:hover {
            background-color: #fff;
            border: 1px solid #55b1df;
            cursor: pointer;
        }

        input[type="submit"].login:focus {
            outline: none;
        }

    </style>
</head>


    <script type="text/javascript">
        $('.alert-message').alert().delay(300).slideUp('slow');

    </script>

    <div id="formWrapper">
        <div id="form">
            <div class="logo">
                <h2>Register</h2>
                
            </div>
            
            <form method="post" action="<?php echo base_url().'welcome/login'; ?>">
                <div class="form-item">
                    <input type="text" name="admin_username" id="email" class="form-style" autocomplete="off" placeholder="username">
                    
                </div>
                <div class="form-item">
                    <input type="password" name="admin_password" id="password" class="form-style" placeholder="password">
                    
                    <!-- <div class="pw-view"><i class="fa fa-eye"></i></div> -->
                </div>
                <div class="form-item">
                    <input type="submit" class="login pull-right" value="Log In">
                </div>
            </form>
        </div>
    </div>
</body>

</html>
