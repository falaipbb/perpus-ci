<head>
    <title>Member - Perpustakaan Online</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chartist/css/chartist-custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css-design/style-card.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css-tmplate/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css-tmplate/demo.css">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/jquery.dataTables.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/datatables.js'; ?>"></script>
    <style type="text/css">
        img {
            padding-top: 0;
            width: 65px;
            height: 65px;
        }

        .navbar-default .brand {
            float: left;
            padding: 5px 39px;
            background-color: #fff;
        }

        li {
            position: relative;
            top: 5px;
            left: -50px;
            text-decoration: none;

        }

        .text-danger {
            position: relative;
            left: 50px;
            top: -9px;
        }

        ul.notifications>li {
            border-bottom: 0px solid #F0F0F0;
        }

    </style>
</head>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="<?php echo base_url();?>member"><img src="<?php echo base_url(); ?>assets/img/logo-bsiedit.png" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <form class="navbar-form navbar-left">
            <ul class="none navbar-nav ml-auto" type="none">
                <li class="nav-item">
                    <a href="<?php echo base_url(). 'member'; ?>">
                        <span class="glyphicon glyphicon-home"></span> Home </a></li>
                </li>
                <li class="nav-item">
                    <a class="nav-link">
                        <?php
        $text_cart_url = '<span class="glyphicon glyphicon-shopping-cart" ariahidden="true"></span>';
        $text_cart_url .= ' Booking Cart: '. $this->m_perpus->edit_data(array('id_anggota'=>$this->session->userdata('id_agt')),'transaksi')->num_rows() .'Buku';
 ?>
                        <?=anchor('peminjaman/lihat_keranjang', $text_cart_url)?></a>
                </li>

        </form>

        </ul>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <?php if($this->session->userdata('id_agt')) { ?>
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                        <?php echo "Halo, <b>".$this->session->userdata('nama_agt');?></b>&nbsp;<i class="fas fa-caret-down"></i></a>
                    <?php } ?>

                    <ul class="dropdown-menu notifications" type="none">
                        <li>
                            <a href="<?php echo base_url().'admin/ganti_password'?>" class="text-danger"><i class="glyphicon glyphicon-lock"></i> Ganti Password</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'admin/logout'; ?>" class="text-danger"><i class="fas fa-times-circle"></i>&nbsp; Logout</a>
                        </li>
                    </ul>
                </li>



        </div>
    </div>
</nav>
