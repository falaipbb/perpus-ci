<!doctype html>
<html lang="en">

<head>
    <title>Data Buku - Perpustakaan Online</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/linearicons/style.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/demo.css">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <style type="text/css">

    </style>

</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <?php $this->load->view('tmplate/navbar'); ?>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <h3 class="page-title">Data Transaksi</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- TABLE HOVER -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a href="<?php echo base_url().'admin/tambah_peminjaman'; ?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span>Transaksi Baru</a>
                                </div>
                                <div class="panel-body" id="table-datatable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Anggota</th>
                                                <th>Judul Buku</th>
                                                <th>Tgl. Pinjam</th>
                                                <th>Tgl. Kembali</th>
                                                <th>Denda / Hari</th>
                                                <th>Tgl. Dikembalikan</th>
                                                <th>Total Denda</th>
                                                <th>Status Buku</th>
                                                <th>Status Pinjam</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
				$no = 1;
				foreach($peminjaman as $p){
			?>
                <tr>
                    <td>
                        <?php echo $no++; ?>
                    </td>
                    <td>
                        <?php echo $p->nama_anggota; ?>
                    </td>
                    <td>
                        <?php echo $p->judul_buku; ?>
                    </td>
                    <td>
                        <?php echo date('d/m/Y',strtotime($p->tgl_pinjam)); ?>
                    </td>
                    <td>
                        <?php echo date('d/m/Y',strtotime($p->tgl_kembali)); ?>
                    </td>
                    <td>
                        <?php echo "Rp. ".number_format($p->denda); ?>
                    </td>
                    <td>
                        <?php
					if($p->tgl_pengembalian =="0000-00-00"){
						echo "-";
					}else{
						echo date('d/m/Y',strtotime($p->tgl_pengembalian));
					}
					?>
                    </td>
                    <td>
                        <?php echo "Rp. ". number_format($p->total_denda)." ,-";	?>
                    </td>
                    <td>
                        <?php
					if($p->status_pengembalian == "1"){
						echo "Kembali";
					}else{
						echo "Belum Kembali";
					}
					?>
                    </td>
                    <td>
                        <?php
					if($p->status_peminjaman == "1"){
						echo "Selesai";
					}else{ ?>
                        <a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/transaksi_selesai/'.$p->id_pinjam; ?>"><span class="glyphicon glyphicon-ok"></span> Transaksi Selesai</a>
                        <br />
                        <a class="btn btn-sm btn-danger" href="<?php echo base_url().'admin/hapus_peminjaman/'.$p->id_pinjam; ?>"><span class="glyphicon glyphicon-remove"></span> Batalin Transaksi</a>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END TABLE HOVER -->
                        </div>

                        <!-- END CONDENSED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->


    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->

</body>

</html>
