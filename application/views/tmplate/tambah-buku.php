<meta charset="utf-8">
   <body class="wrap" style="background-image:url(<?php echo base_url(); ?>assets/img/bg.jpg)">
    <style type="text/css">
        label {
            color: wheat;
        }

        .form-control {
            width: 300px;
            height: 35px;
        }

        .form-inline label {
            padding: 15px;
        }

    </style>
    <div class="page-header">
        <h3>Buku Baru</h3>
    </div>
    <?= validation_errors('<p style="color:red;">','</p>'); ?>
    <?php
if($this->session->flashdata())
	{
		echo "<div class='alert alert-danger alert-message'>";
		echo $this->session->flashdata('alert');
		echo "</div>";
	}
?>
    <form action="<?php echo base_url().'admin/tambah_buku_act' ?>" method="post" enctype="multipart/form-data">
        <div class="form-group form-inline">
            <label>Kategori : </label>
                   <select name="id_kategori" class="form-control">
            <option value="">    -Pilih Kategori-     </option>
                <?php foreach($kategori as $k){ ?>
                <option value="<?php echo $k->id_kategori; ?>">
                    <?php echo $k->nama_kategori; ?>
                </option>
                <?php } ?>
            </select>
            <?php echo form_error('id_kategori'); ?>

            <label> Judul Buku : </label>
            <input type="text" name="judul_buku" class="form-control">
            <?php echo form_error('judul_buku'); ?>

        </div>

        <div class="form-group form-inline">
            <label>Pengarang : </label>
              
            <input type="text" name="pengarang" class="form-control">

            <label> Penerbit : </label>
                
            <input type="text" name="penerbit" class="form-control">
        </div>

        <div class="form-group">
            <label>    Tahun Terbit : </label>
            
            <input type="date" name="thn_terbit" class="form-control">
        </div>

        <div class="form-group form-inline">
            <label> ISBN : </label>
                        
            <input type="text" name="isbn" class="form-control">

            <label> Lokasi : </label>
                   
            <input type="text" name="lokasi" class="form-control">
        </div>

        <div class="form-group form-inline">
            <label>Jumlah Buku : </label>
            <input type="text" name="jumlah_buku" class="form-control">

            <label>Status Buku :</label>
            
            <select name="status" class="form-control">
                <option value="1">Tersedia</option>
                <option value="0">Sedang Di Pinjam</option>
            </select>
            <?php echo form_error('status'); ?>
        </div>

        <div class="form-group">
            <label>    Gambar : </label>
            <input name="foto" type="file" class="form-control">
        </div>

        <div class="form-group">
              
           <br>
            <input type="submit" value="Simpan" class="btn btn-primary">
        </div>

    </form>

</body>
