    <style type="text/css">
        div .thumbnail img {
            height: 300px;
            width: 250px;
            padding: 0;

        }

        div .thumbnail {
            padding: 0;
        }
        
        .page-header{
            padding-left: 10px;
            position: relative;
            top: -20px;
        }

    </style>

</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->

        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <?php if($this->session->flashdata())
        {
            echo "<div class='alert alert-danger alert-primary'>";
            echo $this->session->flashdata('alert');
            echo "</div>";
        } ?>
           <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel">
                <div class="panel-body">
                    <div class="row">

                        <div class="page-header">
                            <h3>Katalog Buku</h3>
                            <p class="panel-subtitle"><i class="fab fa-steam">&nbsp;Edit : Jipay</p></i>
                        </div>
                        <?php foreach($data_buku as $buku) { ?>
                        <div class="col-sm-3 col-md-3">
                            <!-- PANEL NO PADDING -->

                            <div class="thumbnail">
                                <img src="<?php echo base_url();?>assets/upload/<?=$buku->gambar; ?>">


                                <div class="caption">
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6 text-left">
                                                <?=anchor('peminjaman/tambah_pinjam/' . $buku->id_buku, ' Booking' , [
                                             'class' => 'btn btn-danger glyphicon glyphicon-save',
                                             'role' => 'button'
                                        ])?>    
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <?=anchor('buku/katalog_detail/' . $buku->id_buku, ' Detail' , [
                                             'class' => 'btn btn-primary glyphicon glyphicon-zoom-in',
                                             'role' => 'button'
                                        ])?>
                                                </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END PANEL NO PADDING -->
                        </div>
                        <?php } ?>
                    </div>

                    <!-- END PANEL SCROLLING -->
                </div>
            </div>
        </div>
    
    </div>
    <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>

</body>

</html>
