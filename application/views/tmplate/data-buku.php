<style type="text/css">
    .panel{
        position: relative;
        top: -10px;
    }
    .page-header{
        position: relative;
        top: 40px;
    }
</style>
    <!-- WRAPPER -->

        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="page-header">
                    <h3 class="page-title">Data Buku</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- TABLE HOVER -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a href="<?php echo base_url().'admin/tambah_buku'; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span> Buku Baru</a>
                                </div>
                                <div class="panel-body" id="table-datatable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Gambar</th>
                                                <th>Judul Buku</th>
                                                <th>Pengarang</th>
                                                <th>Penerbit</th>
                                                <th>Tahun Terbit</th>
                                                <th>ISBN</th>
                                                <th>Lokasi</th>
                                                <th>Status</th>
                                                <th>Pilihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
				$no = 1;
				foreach($buku as $b){
			?>
                                            <tr>
                                                <td>
                                                    <?php echo $no++; ?>
                                                </td>
                                                <td><img src="<?php echo base_url().'/assets/upload/'.$b->gambar; ?>" width="90" height="70" alt="gambar tidak ada"></td>
                                                <td>
                                                    <?php echo $b->judul_buku ?>
                                                </td>
                                                <td>
                                                    <?php echo $b->pengarang ?>
                                                </td>
                                                <td>
                                                    <?php echo $b->penerbit ?>
                                                </td>
                                                <td>
                                                    <?php echo $b->thn_terbit ?>
                                                </td>
                                                <td>
                                                    <?php echo $b->isbn ?>
                                                </td>
                                                <td>
                                                    <?php echo $b->lokasi ?>
                                                </td>
                                                <td>
                                                    <?php
						if($b->status_buku == "1"){
							echo "Tersedia";
						}else if($b->status_buku == "0"){
							echo "Sedang Di Pinjam";
						}
					?>
                                                </td>
                                                <td nowrap="nowrap">
                                                    <a class="btn btn-primary btn-xs" href="<?php echo base_url().'admin/edit_buku/'.$b->id_buku; ?>"><span class="glyphicon glyphicon-zoom-in"></span></a>
                                                    <a class="btn btn-danger btn-xs" href="<?php echo base_url().'admin/hapus_buku/'.$b->id_buku; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END TABLE HOVER -->
                        </div>

                        <!-- END CONDENSED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->


    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->

</body>

</html>
