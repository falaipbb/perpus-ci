<!doctype html>
<html lang="en">

<head>
    <title>Dashboard - Perpustakaan Online</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/chartist/css/chartist-custom.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fontawesome/css/all.min.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css-tmplate/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css-tmplate/demo.css">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    
</head>

<body>

    <!-- WRAPPER -->
    <?php $this->load->view('tmplate/navbar'); ?>
    <div id="wrapper">
        <!-- NAVBAR -->
        <!--include header-->

        <!-- END NAVBAR -->
        <!-- LEFT SIDEBAR -->

        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <!-- OVERVIEW -->
                    <div class="panel panel-headline">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dashboard</h3>
                            <p class="panel-subtitle"><i class="fab fa-steam">&nbsp;Edit : Jipay</p></i>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-book"></i></span>
                                        <p>
                                            <span class="number">
                                                <?php echo $this->m_perpus->get_data('buku')->num_rows(); ?></span>
                                            <span class="title">Jumlah Buku Yang Terdaftar</span>
                                        </p>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i>Last 24 hours</span></div>
                                            <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/buku' ?>" class="btn btn-primary">View All</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><i class="fa fa-user"></i></span>
                                        <p>
                                            <span class="number">
                                                <?php echo $this->m_perpus->get_data('anggota')->num_rows(); ?></span>
                                            <span class="title">Jumlah Anggota Yang Terdaftar</span>
                                        </p>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                            <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/anggota' ?>" class="btn btn-primary">View All</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><i class="far fa-calendar-times"></i></span>
                                        <p>
                                            <span class="number">
                                                <?php echo $this->m_perpus->edit_data(array('status_peminjaman'=>0),'transaksi')->num_rows(); ?></span>
                                            <span class="title">Peminjaman Belum Selesai</span>
                                        </p>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                            <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/peminjaman'; ?>" class="btn btn-primary">View All</a></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="metric">
                                        <span class="icon"><i class="far fa-calendar-check"></i></span>
                                        <p>
                                            <span class="number">
                                                <?php echo $this->m_perpus->edit_data(array('status_peminjaman'=>1),'transaksi')->num_rows(); ?></span>
                                            <span class="title">Peminjaman Sudah Selesai</span>
                                        </p>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                            <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/peminjaman'; ?>" class="btn btn-primary">View All</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div id="headline-chart" class="ct-chart"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END OVERVIEW -->
                    
                            <!-- END RECENT PURCHASES -->
                           

                            <!-- END TASKS -->
                        </div>
                        
                        <div class="col-md-4">
                            <!-- REALTIME CHART -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fas fa-swatchbook">&nbsp;Buku</h3></i>
                                    <div class="right">

                                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>

                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php foreach($buku as $b){ ?>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">
                                            <?php if($b->status_buku == 1){echo "Tersedia";}else{echo "Dipinjam";}?></span>
                                        <i class="glyphicon glyphiconuser"></i>
                                        <?php echo $b->judul_buku; ?>
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                        <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/buku' ?>" class="btn btn-primary">View All</a></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- REALTIME CHART -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fas fa-users">&nbsp;Anggota Terbaru</h3></i>
                                    <div class="right">
                                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php foreach($anggota as $a){ ?>
                                    <a href="#" class="list-group-item">
                                        <span class="badge">
                                            <?php echo $a->gender; ?></span>
                                        <i class="glyphicon glyphiconuser"></i>
                                        <?php echo $a->nama_anggota; ?>
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                        <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/anggota' ?>" class="btn btn-primary">View All</a></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-4">
                            <!-- RECENT PURCHASES -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="far fa-clipboard">&nbsp;Peminjaman Terakhir</h3></i>
                                    <div class="right">
                                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tgl. Transaksi</th>
                                                <th>Tgl. Pinjam</th>
                                                <th>Tgl. Kembali</th>
                                                <th>Total Denda</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
									foreach($peminjaman as $p){
								            ?>
                                            <tr>
                                                <td>
                                                    <?php echo date('d/m/Y',strtotime($p->tgl_pencatatan)); ?>
                                                </td>
                                                <td>
                                                    <?php echo date('d/m/Y',strtotime($p->tgl_pinjam)); ?>
                                                </td>
                                                <td>
                                                    <?php echo date('d/m/Y',strtotime($p->tgl_kembali)); ?>
                                                </td>
                                                <td>
                                                    <?php echo "Rp.".number_format($p->total_denda)." ,-"; ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                        <div class="col-md-6 text-right"><a href="<?php echo base_url().'admin/peminjaman' ?>" class="btn btn-primary">View All</a></div>
                                    </div>
                                </div>
                            </div>
                        <!-- END REALTIME CHART -->
                    </div>
                </div>
            </div>
        </div>
        
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
<!-- Javascript -->
	
</body>

</html>
