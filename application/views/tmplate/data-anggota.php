<!doctype html>
<html lang="en">

<head>
    <title>Data Buku - Perpustakaan Online</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/linearicons/style.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/demo.css">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->

</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <?php $this->load->view('tmplate/navbar'); ?>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <h3 class="page-title">Data Anggota</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- TABLE HOVER -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <a href="<?php echo base_url().'admin/tambah_anggota'; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span> Anggota Baru</a>
                                </div>
                                <div class="panel-body" id="table-datatable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Anggota</th>
                                                <th>Jenis Kelamin</th>
                                                <th>No. Telp</th>
                                                <th>Alamat</th>
                                                <th>Email</th>
                                                <th>Pilihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
				$no = 1;
				foreach($anggota as $b){
			?>
                <tr>
                    <td>
                        <?php echo $no++; ?>
                    </td>
                    <td>
                        <?php echo $b->nama_anggota ?>
                    </td>
                    <td>
                        <?php echo $b->gender ?>
                    </td>
                    <td>
                        <?php echo $b->no_telp ?>
                    </td>
                    <td>
                        <?php echo $b->alamat ?>
                    </td>
                    <td>
                        <?php echo $b->email ?>
                    </td>
                    <td nowrap="nowrap">
                        <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/edit_anggota/'.$b->id_anggota; ?>"><span class="glyphicon glyphicon-zoom-in"> </span></a>
                        <a class="btn btn-danger btn-sm" href="<?php echo base_url().'admin/hapus_anggota/'.$b->id_anggota; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END TABLE HOVER -->
                        </div>

                        <!-- END CONDENSED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->


    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->

</body>

</html>
