<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css-design/landing-style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/sweetalert.min.js">
  </head>
  <body>

    <div class="landing-page">
      <div class="page-content">
        <h1>Welcome To My Web :)</h1>
        <p>
          “Manisnya akhirat mustahil diraih oleh orang-orang yang lebih suka terkenal di mata manusia”

        </p>
        <a href="<?php echo base_url();?>welcome">Get Started</a>
        
      </div>
    </div>


  </body>
</html>
