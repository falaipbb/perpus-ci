<!DOCTYPE html>
<html>

<head>
    <title>Dashboard - Aplikasi Perpustakaan</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/datatable/datatables.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fontawesome/css/fontawesome.min.css" >
    <link href="<?php echo base_url(); ?>assets/css-design/style-web-crud.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/jquery.dataTables.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/datatables.js'; ?>"></script>

    <style type="text/css">
        .navbar-nav>li>a {
            font-size: 20px;
            color: wheat;
            
        }
        
        .navbar-nav>li>a:hover{
            color: blue;
            text-decoration: none;
        }
        
        .btn-group{
            position: absolute;
        }
        
        .btn-primary{
            position: absolute;
            left: 650px;
        }
        
        div .thumbnail{
            border-radius: 10px;
        }
        
        h2{
            color: white;
            font-family: times new roman;
        }
    </style>
</head>

<nav class="navbar navbar-expand-sm">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-brand">
            <a class="navbar-brand" href="<?php echo base_url(); ?>Welcome"><img src="<?php echo base_url(); ?>assets/img/logo-bsiedit.png"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(). 'member'; ?>">
                        <span class="glyphicon glyphicon-home"></span> Home </a></li>
                <li class="nav-item">
                    <a class="nav-link">
                        <?php
        $text_cart_url = '<span class="glyphicon glyphicon-shopping-cart" ariahidden="true"></span>';
        $text_cart_url .= ' Booking Cart: '. $this->m_perpus->edit_data(array('id_anggota'=>$this->session->userdata('id_agt')),'transaksi')->num_rows() .'Buku';
 ?>
                        <?=anchor('peminjaman/lihat_keranjang', $text_cart_url)?></a>
                </li>
            </ul>
            <div class="btn-group">
                <?php if($this->session->userdata('id_agt')) { ?>
                <button type="button" class="btn btn-primary">
                    <?php echo "Halo, <b>".$this->session->userdata('nama_agt');?></a>
                </button>
                <?php } else { ?>
                <li>
                    <?php echo anchor('welcome', 'Login');?>
                </li>
                <?php } ?>

                <div class="btn-group">
                    <a href="<?php echo base_url().'admin/logout'; ?>" class="btn btn-primary">Logout</a>
                </div><!-- /.navbar-collapse -->
            </div>
        </div>
        <!-- /.container-fluid -->
</nav>
