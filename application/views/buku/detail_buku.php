<style type="text/css">
    .thumbnail {
        top: 80px;
    }

    h3 {

        color: black;
        text-align: center;
    }

    .panel {
        position: relative;
        top: 80px;
    }

    .thumbnail>img {
        margin-right: auto;
        margin-left: auto;
        width: 1000px;
        height: 400px;
    }

    .thumbnail {
        padding: 0;
        border: 5px groove chocolate;
    }

    table {
        position: absolute;
        left: 20px;
        top: 150px;
    }

    table>td {
        border-top: 0px solid #ddd;
        color: red;
    }

    p {
        position: relative;
        top: 400px;
        left: 30px;
    }

    .panel .panel-body {
        padding-left: 100px;
        padding-right: 25px;

</style>


<div class="main-content">
    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel">
            <div class="panel-body">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <h3>Detail Buku</h3>
                            <a href="<?php echo base_url();?>assets/upload/<?=$gambar;?>" class="thumbnail">
                                <img src="<?php echo base_url();?>assets/upload/<?=$gambar;?>">
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-hover">

                                <tr>
                                    <td><i class="fas fa-book"></i>&nbsp;&nbsp;Pengarang</td>
                                    <td>
                                        <?=$pengarang?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fas fa-bookmark"></i>&nbsp;&nbsp;Judul Buku </td>
                                    <td>
                                        <?=substr($judul,0,50)?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fas fa-print"></i>&nbsp;&nbsp;Penerbit </td>
                                    <td>
                                        <?=$penerbit?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fas fa-folder-open"></i>&nbsp;Tahun Terbit </td>
                                    <td>
                                        <?=substr($tahun,0,4)?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fas fa-swatchbook"></i>&nbsp;&nbsp;ISBN </td>
                                    <td>
                                        <?=$isbn?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fas fa-tags"></i>&nbsp;&nbsp;Kategori </td>
                                    <td>
                                        <?=$kategori?>
                                    </td>
                                </tr>

                                <p>
                                   
                                    <?=anchor('peminjaman/tambah_pinjam/' . $id, ' Booking' , [
            'class' => 'btn btn-success',
            'role' => 'button'
            ])?>
                                <a href="<?php echo base_url();?>member" class="btn btn-danger"><i class="fas fa-caret-left"></i>&nbsp; Kembali</a>
                                </p>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</body>
