<style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 106%;
        position: absolute;
        top: 80px;
        left: -30px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 4px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>



</style>
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="page-header">
                Laporan Data Anggota Perpustakaan Online
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-6">

                    <div class="box">
                        <div class="box-body">
                            <table>
                                
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Anggota</th>
                                        <th>Jenis Kelamin</th>
                                        <th>No. Telp</th>
                                        <th>Alamat</th>
                                        <th>Email</th>
                                    </tr>
                                    <?php
            $no = 1;
            foreach ($anggota as $b) { 
            ?>
                                    <tr>
                                        <td>
                                            <?php echo $no++; ?>
                                        </td>
                                        <td>
                                            <?php echo $b->nama_anggota; ?>
                                        </td>
                                        <td>
                                            <?php echo $b->gender; ?>
                                        </td>
                                        <td>
                                            <?php echo $b->no_telp; ?>
                                        </td>
                                        <td>
                                            <?php echo $b->alamat; ?>
                                        </td>
                                        <td>
                                            <?php echo $b->email; ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                

                            </table>


                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</div>
