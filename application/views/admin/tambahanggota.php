<meta charset="utf-8">


    <style type="text/css">
      
        .row{
            position: relative;
            top: -15px;
            padding-left: 20px;
        }

        .form-control {
            width: 350px;
            height: 30px;

        }

        .container {
            position: relative;
            top: 55px;
        }

        .form-inline label {
            padding: 15px;
        }

        .panel{
            position: relative;
            top: 70px;
        }

        .radio {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 13px;
            font-family: monospace;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* hide the browser's default radio button */
        .radio input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* create custom radio */
        .radio .check {
            position: absolute;
            top: 0;
            left: 0;
            height: 16px;
            width: 16px;
            background-color: #eee;
            border: 1px solid #ccc;
            border-radius: 50%;
        }

        /* on mouse-over, add border color */
        .radio:hover input~.check {
            border: 2px solid #2489C5;
        }

        /* add background color when the radio is checked */
        .radio input:checked~.check {
            background-color: #2489C5;
            border: none;
        }

        /* create the radio and hide when not checked */
        .radio .check:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* show the radio when checked */
        .radio input:checked~.check:after {
            display: block;
        }

        /* radio style */
        .radio .check:after {
            top: 4px;
            left: 4px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
        }

    </style>


    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">

                            <div class="page-header">
                                <h3>Tambah Anggota</h3>
                                <p class="panel-subtitle"><i class="fab fa-steam">.Edit : Jipay</p></i>
                            </div>
                            <form action="<?php echo base_url().'admin/tambah_anggota_act' ?>" method="post">
                                <div class="form-group">
                                    <label>Nama Anggota</label>
                                    <input class="form-control" type="text" name="nama_anggota">
                                    <?php echo form_error('nama_anggota'); ?>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password">
                                </div>

                                <div class="form-group">
                                    <label>Ulangi Password</label>
                                    <input class="form-control" type="password" name="ulangi_password">
                                </div>

                                <div class="form-group">
                                    <h4>Jenis Kelamin</h2>
                                        <label class="radio">Laki-Laki
                                            <input type="radio" name="gender" value="Laki-Laki">
                                            <span class="check"></span>
                                        </label>
                                        <label class="radio">Perempuan
                                            <input type="radio" name="gender" value="Perempuan">
                                            <span class="check"></span>
                                        </label>
                                </div>

                                <div class="form-group">
                                    <label>No Telp</label>
                                    <input class="form-control" type="text" name="notelp">
                                </div>

                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input class="form-control" type="textarea" name="alamat">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email">
                                    <?php echo form_error('email'); ?>
                                </div>

                                <div class="form-group">
                                    <input type="submit" value="Simpan" class="btn btn-primary">
                                </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
