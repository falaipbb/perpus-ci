<style type="text/css">
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 106%;
        position: absolute;
        top: 80px;
        left: -30px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 4px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>



</style>
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="page-header">
                Laporan Data Buku Perpustakaan Online
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-6">

                    <div class="box">
                        <div class="box-body">
                            <table>
                                
        <tr>
            <th>No</th>
            <th>Judul Buku</th>
            <th>Pengarang</th>
            <th>Penerbit</th>
            <th>Tahun Terbit</th>
            <th>ISBN</th>
            <th>Jumlah Buku</th>
            <th>Lokasi</th>
        </tr>
            <?php
            $no = 1;
            foreach ($buku as $b) {
            ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $b->judul_buku ?></td>
                <td><?php echo $b->pengarang ?></td>
                <td><?php echo $b->penerbit ?></td>
                <td><?php echo $b->thn_terbit ?></td>
                <td><?php echo $b->isbn ?></td>
                <td><?php echo $b->jumlah_buku; ?></td>
                <td><?php echo $b->lokasi ?></td>
            </tr>
            <?php } ?>
        
                            </table>


                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</div>
