<meta charset="utf-8">


    <style type="text/css">
        .panel{
            position: relative;
            top: 80px;
        }

        .form-control {
            width: 300px;
            height: 35px;
        }
        .form-group {
            padding-left: 30px;
        }
        .page-header{
            padding-left: 30px;
            
        }
        .row{
            position: relative;
            top: -10px;
        }

    </style>
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel">
                    <div class="row">
                        <div class="page-header">
                            <h3>Buku Baru</h3>
                            <p class="panel-subtitle"><i class="fab fa-steam">.Edit : Jipay</p></i>
                        </div>
                        <?= validation_errors('<p style="color:red;">','</p>'); ?>
                        <?php
if($this->session->flashdata())
	{
		echo "<div class='alert alert-danger alert-message'>";
		echo $this->session->flashdata('alert');
		echo "</div>";
	}
?>
                        <form action="<?php echo base_url().'admin/tambah_buku_act' ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group form-inline">
                                <label>Kategori : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name="id_kategori" class="form-control">
                                    <option value="">&nbsp;&nbsp;&nbsp;&nbsp;-Pilih Kategori-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                    <?php foreach($kategori as $k){ ?>
                                    <option value="<?php echo $k->id_kategori; ?>">
                                        <?php echo $k->nama_kategori; ?>
                                    </option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('id_kategori'); ?>

                                <label>&nbsp;Judul Buku : </label>
                                &nbsp;
                                <input type="text" name="judul_buku" class="form-control">
                                <?php echo form_error('judul_buku'); ?>

                            </div>

                            <div class="form-group form-inline">
                                <label>Pengarang : </label>
                                &nbsp;&nbsp;
                                <input type="text" name="pengarang" class="form-control">

                                <label>&nbsp;Penerbit : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" name="penerbit" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>&nbsp;&nbsp;&nbsp;&nbsp;Tahun Terbit : </label>

                                <input type="date" name="thn_terbit" class="form-control">
                            </div>

                            <div class="form-group form-inline">
                                <label>&nbsp;ISBN : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" name="isbn" class="form-control">

                                <label>&nbsp;Lokasi : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" name="lokasi" class="form-control">
                            </div>

                            <div class="form-group form-inline">
                                <label>Jumlah Buku : </label>
                                <input type="text" name="jumlah_buku" class="form-control">

                                <label>Status Buku :</label>

                                <select name="status" class="form-control">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Sedang Di Pinjam</option>
                                </select>
                                <?php echo form_error('status'); ?>
                            </div>

                            <div class="form-group">
                                <label>&nbsp;&nbsp;&nbsp;&nbsp;Gambar : </label>
                                <input name="foto" type="file" class="form-control">
                            </div>

                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;
                                <br>
                                <input type="submit" value="Simpan" class="btn btn-primary">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>
