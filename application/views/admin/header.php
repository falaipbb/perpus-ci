<!DOCTYPE html>
<html>

<head>
    <title>Dashboard - Aplikasi Perpustakaan</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/datatable/datatables.css' ?>">
    <link href="<?php echo base_url(); ?>assets/css-design/style-web-crud.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/jquery.dataTables.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/datatable/datatables.js'; ?>"></script>
</head>


    <nav class="navbar navbar-expand-sm">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a class="navbar-brand" href="<?php echo base_url(); ?>Welcome"><img src="<?php echo base_url(); ?>assets/img/logo-bsiedit.png"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(). 'admin'; ?>">
                            <span class="glyphicon glyphicon-home"></span> Dashboard </a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'admin/buku'; ?>">
                            <span class="glyphicon glyphicon-folder-open"></span> Data Buku</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'admin/anggota'; ?>">
                            <span class="glyphicon glyphicon-user"></span> Data Anggota</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'admin/peminjaman'; ?>">
                            <span class="glyphicon glyphicon-sort"></span> Transaksi Peminjaman</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'admin/laporan'; ?>">
                            <span class="glyphicon glyphicon-list-alt"></span> Laporan</a></li>
                </ul>
              
                <div class="btn-group">
                    <a href="<?php echo base_url().'admin/logout'; ?>" class="btn btn-primary">Logout</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <?php echo "Halo, <b>".$this->session->userdata('nama');?></b> <span class="caret"></span></a>
                        </button>
                        <div class="dropdown-menu">
                            <a href="<?php echo base_url().'admin/ganti_password'?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-lock"></i> Ganti Password</a>
                        </div>
                    </div>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    
    <div class="container">
