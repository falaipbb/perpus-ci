<body class="wrap" style="background-image:url(<?php echo base_url(); ?>assets/img/bg-black.jpg)">
    <style type="text/css">
        .table {
            font-size: 15px;
            color: wheat;
            text-align: center;
        }

        .table-condensed thead tr th {
            text-align: center;
            font-size: 15px;
            font-family: fantasy;
        }

        .btn-xsi {
            color: wheat;
            width: 100px;
            height: 30px;
            padding: 3px;
            padding-right: 4px;
            font-size: 14px;

        }

        label {
            color: wheat;
            border: none;
            font-size: 17px;

        }

        input {
            width: 100px;
        }

        div.dataTables_filter input {
            width: 180px;
            color: black;
            border: 1px solid;
            border-radius: 100px;
        }

        div.dataTables_length select {
            width: 45px;
            color: black;
            border: 1px solid;
            border-radius: 100px;
        }

        div.dataTables_info {
            color: wheat;
        }

    </style>
                           
                            <div class="page-header">
                                <h3>Data Transaksi</h3>
                            </div>


                            <a href="<?php echo base_url().'admin/tambah_peminjaman'; ?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Transaksi Baru</a>
                            <br /><br />
                            <div>
                                <table class="table table-condensed" id="table-datatable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Anggota</th>
                                            <th>Judul Buku</th>
                                            <th>Tgl. Pinjam</th>
                                            <th>Tgl. Kembali</th>
                                            <th>Denda / Hari</th>
                                            <th>Tgl. Dikembalikan</th>
                                            <th>Total Denda</th>
                                            <th>Status Buku</th>
                                            <th>Status Pinjam</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
				$no = 1;
				foreach($peminjaman as $p){
			?>
                                        <tr>
                                            <td>
                                                <?php echo $no++; ?>
                                            </td>
                                            <td>
                                                <?php echo $p->nama_anggota; ?>
                                            </td>
                                            <td>
                                                <?php echo $p->judul_buku; ?>
                                            </td>
                                            <td>
                                                <?php echo date('d/m/Y',strtotime($p->tgl_pinjam)); ?>
                                            </td>
                                            <td>
                                                <?php echo date('d/m/Y',strtotime($p->tgl_kembali)); ?>
                                            </td>
                                            <td>
                                                <?php echo "Rp. ".number_format($p->denda); ?>
                                            </td>
                                            <td>
                                                <?php
					if($p->tgl_pengembalian =="0000-00-00"){
						echo "-";
					}else{
						echo date('d/m/Y',strtotime($p->tgl_pengembalian));
					}
					?>
                                            </td>
                                            <td>
                                                <?php echo "Rp. ". number_format($p->total_denda)." ,-";	?>
                                            </td>
                                            <td>
                                                <?php
					if($p->status_pengembalian == "1"){
						echo "Kembali";
					}else{
						echo "Belum Kembali";
					}
					?>
                                            </td>
                                            <td>
                                                <?php
					if($p->status_peminjaman == "1"){
						echo "Selesai";
					}else{ ?>
                                                <a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/transaksi_selesai/'.$p->id_pinjam; ?>"><span class="glyphicon glyphicon-ok"></span> Transaksi Selesai</a>
                                                <br />
                                                <a class="btn btn-sm btn-danger" href="<?php echo base_url().'admin/hapus_peminjaman/'.$p->id_pinjam; ?>"><span class="glyphicon glyphicon-remove"></span> Batalkan Transaksi</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
