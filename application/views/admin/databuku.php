<meta charset="utf-8">

<body class="wrap" style="background-image:url(<?php echo base_url(); ?>assets/img/bg-black.jpg)">
    <style type="text/css">
        .table {
            font-size: 15px;
            color: wheat;
            text-align: center;
        }

        .table-condensed thead tr th {
            text-align: center;
            font-size: 15px;
            font-family: fantasy;
        }

  

        label {
            color: wheat;
            border: none;
            font-size: 17px;

        }

        input {
            width: 100px;
        }

        div.dataTables_filter input {
            width: 180px;
            color: black;
            border: 1px solid;
            border-radius: 100px;
        }

        div.dataTables_length select {
            width: 45px;
            color: black;
            border: 1px solid;
            border-radius: 100px;
        }

        div.dataTables_info {
            color: wheat;
        }

        .pagination>li:first-child>a,
        .pagination>li:first-child>span {
            color: black;
        }

        .pagination>li:last-child>a,
        .pagination>li:last-child>span {
            color: black;
        }

        img {
            vertical-align: middle;
            width: 65px;
            position: relative;
            left: 5px;
        }

    </style>
    <div class="page-header">
        <h3>Data Buku</h3>
    </div>
    <a href="<?php echo base_url().'admin/tambah_buku'; ?>" class="btn btn-primary btn-xsi"><span class="glyphicon glyphicon-plus"></span> Buku Baru</a>
    <br /><br />
    <div class="table-responsive">
        <table class="table table-condensed" id="table-datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Judul Buku</th>
                    <th>Pengarang</th>
                    <th>Penerbit</th>
                    <th>Tahun Terbit</th>
                    <th>ISBN</th>
                    <th>Lokasi</th>
                    <th>Status</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
				$no = 1;
				foreach($buku as $b){
			?>
                <tr>
                    <td>
                        <?php echo $no++; ?>
                    </td>
                    <td><img src="<?php echo base_url().'/assets/upload/'.$b->gambar; ?>" width="90" height="70" alt="gambar tidak ada"></td>
                    <td>
                        <?php echo $b->judul_buku ?>
                    </td>
                    <td>
                        <?php echo $b->pengarang ?>
                    </td>
                    <td>
                        <?php echo $b->penerbit ?>
                    </td>
                    <td>
                        <?php echo $b->thn_terbit ?>
                    </td>
                    <td>
                        <?php echo $b->isbn ?>
                    </td>
                    <td>
                        <?php echo $b->lokasi ?>
                    </td>
                    <td>
                        <?php
						if($b->status_buku == "1"){
							echo "Tersedia";
						}else if($b->status_buku == "0"){
							echo "Sedang Di Pinjam";
						}
					?>
                    </td>
                    <td nowrap="nowrap">
                        <a class="btn btn-primary btn-xs" href="<?php echo base_url().'admin/edit_buku/'.$b->id_buku; ?>"><span class="glyphicon glyphicon-zoom-in"></span></a>
                        <a class="btn btn-danger btn-xs" href="<?php echo base_url().'admin/hapus_buku/'.$b->id_buku; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
