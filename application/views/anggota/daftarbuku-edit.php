<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Perpustakaan</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css-design/card.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js "></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/bootstrap.js'; ?>"></script>

    <style type="text/css">
        .card {
            position: relative;
            width: 270px;
            height: 370px;
            background: #262626;
            overflow: hidden;
        }

        input,
        .toggle {
            position: absolute;
            width: 50px;
            height: 50px;
            bottom: 20px;
            right: 20px;
            outline: none;
            z-index: 10;
        }

        input {
            opacity: 0;
        }

        .toggle {
            pointer-events: none;
            border-radius: 50%;
            background: #fff;
            transition: 0.5s;
            text-align: center;
            line-height: 50px;
            font-size: 36px;
            box-shadow: 0 0 0 0px #9c27b0;
        }

        input:checked~.toggle {
            box-shadow: 0 0 0 500px #9c27b0;
            transform: rotate(225deg);
        }

        .imgbx,
        .details {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .details {
            padding: 20px;
            box-sizing: border-box;
            z-index: 11;
            pointer-events: none;
            transition: 0.5s;
            opacity: 0;
        }

        input:checked~.details {
            opacity: 1;
            transition-delay: 0.5s;
        }

        .details h2 {
            margin-bottom: 5px;
            color: #fff;
        }

        .details p {
            margin: 0;
            padding: 0;
            color: #fff;
        }

    </style>


</head>

<body class="wrap" style="background-image:url(<?php echo base_url(); ?>assets/img/library.jpg)">
    <div>
        <?php $this->load->view('anggota/toplayout'); ?>
    </div>
    <?php if($this->session->flashdata())
        {
            echo "<div class='alert alert-danger alert-primary'>";
            echo $this->session->flashdata('alert');
            echo "</div>";
        } ?>
    <div class="container">
        <div class="x_panel">
            <div class="x_title">
                <div class="page-header">
                    <h3>
                        <?php echo $header; ?>
                    </h3>
                </div>
            </div>
            
            <div class="x_content">
                <!-- Tampilkan semua produk -->
                <div class="row">
                    <!-- looping products -->
                    <?php foreach($data_buku as $buku) { ?>
                    <div class="col-sm-3 col-md-3">
                    <div class="thumbnail">
                    <img src="<?php echo base_url();?>assets/upload/<?=$buku->gambar; ?>">
                    <div class="card">
                        <input type="checkbox" name="">
                        <div class="toggle">+</div>
                        
                        
                        <div class="details">
                            <h2>
                                <?=$buku->pengarang?>
                            </h2>
                            <p>
                                <?=substr($buku->judul_buku,0,30)?>
                            </p>
                            <p>
                                <?=$buku->penerbit?>
                            </p>
                            <p>
                                <?=substr($buku->thn_terbit,0,4)?>
                            </p>
                            <button type="button">
                                <?=anchor('peminjaman/tambah_pinjam/' . $buku->id_buku, '
     Booking' , [
         'class' => 'btn btn-info',
         'role' => 'button'
    ])?>
                            </button>
                            <button type="button">
                                <?=anchor('buku/katalog_detail/' . $buku->id_buku, ' Detail' , [
         'class' => 'btn btn-warning glyphicon glyphicon-zoom-in',
         'role' => 'button'
    ])?>
                            </button>
                        </div>
                        <?php } ?>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
