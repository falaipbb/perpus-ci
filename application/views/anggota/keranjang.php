<style type="text/css">
    .container {
        position: relative;
        top: 80px;
        left: 10px;
    }

    .panel .panel-body {
        padding-left: 60px;
        padding-right: 25px;
    }
    
    
    
 
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container">
            <!-- OVERVIEW -->
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="page-header">
                            <h3>Data Peminjam</h3>
                        </div>
                        <?php foreach ($anggota as $a) { ?>
                        <div class="card middle">
                            <div class="front">
                                <img src="<?php echo base_url(); ?>assets/img/bg-web.jpg" alt="">
                            </div>
                            <div class="back">
                                <div class="back-content middle">
                                    <table class="table">

                                        <tr>
                                            <th>Nama Peminjam </th>
                                            
                                            <td>
                                                <?php echo $a->nama_anggota;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Alamat </th>
                                            
                                            <td>
                                                <?php echo $a->alamat; ?>
                                            </td>
                                        </tr>

                                        <?php } ?>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>


                        <td colspan="6">
                            <br /><br />
                            <div class="page-header">
                                <h3>Data Buku</h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="table-datatable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Gambar</th>
                                            <th>Judul Buku</th>
                                            <th>Pengarang</th>
                                            <th>Penerbit</th>
                                            <th>Tahun</th>
                                            <th>Pilihan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($peminjaman as $b){
 ?>
                                        <tr>
                                            <td>
                                                <?php echo $no++; ?>
                                            </td>
                                            <td><img src="<?php echo base_url();?>assets/upload/<?php echo $b->gambar; ?>" width="70"></td>
                                            <td>
                                                <?php echo $b->judul_buku; ?>
                                            </td>
                                            <td>
                                                <?php echo $b->pengarang; ?>
                                            </td>
                                            <td>
                                                <?php echo $b->penerbit; ?>
                                            </td>
                                            <td>
                                                <?php echo $b->thn_terbit; ?>
                                            </td>
                                            </form>
                                            <td>
                                                <a class="btn btn-sm btn-danger" href="<?php echo
base_url().'peminjaman/hapus_keranjang/'.$b->id_buku; ?>"><span class="glyphicon
glyphicon-remove"></span> </a>
                                                <br />
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <a class="btn btn-sm btn-primary" href="<?php echo
base_url().'member'; ?>"><span class="glyphicon glyphicon-delete"></span>
                                    Lanjutkan Booking Buku</a>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right">
                                <a class="btn btn-sm btn-success" href="<?php echo
base_url().'peminjaman/selesai_booking/'.$this->session->userdata('id_agt');
?>"><span class="glyphicon glyphicon-delete"></span> Selesaikan Booking</a>
                            </td>
                        </tr>
                        </table>
                    </div>
                </div>
