<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Perpustakaan - login anggota</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css-design/card.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js "></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url(). 'assets/js/bootstrap.js'; ?>"></script>

    <style type="text/css">
        .container {
            position: relative;
            top: -35px;
        }

        div .thumbnail {
            height: 300px;
            padding: 0px;
            border: 3px groove white;
            box-shadow: 10px;
            width: 250px;
        }
        
        div .thumbnail img{
            height: 294px;
        }
        
        .caption{
            position: absolute;
            top: 250px;
            left: 50px;
        }

    </style>


</head>

<body class="wrap" style="background-image:url(<?php echo base_url(); ?>assets/img/bg-web.jpg)">
    <div>
        <?php $this->load->view('tmplate/navbar-anggota'); ?>
    </div>
    <?php if($this->session->flashdata())
        {
            echo "<div class='alert alert-danger alert-primary'>";
            echo $this->session->flashdata('alert');
            echo "</div>";
        } ?>
    <div class="container">
        <div class="x_panel">
            <div class="x_title">
                <div class="page-header">
                    <h3>
                        <?php echo $header; ?>
                    </h3>
                </div>
            </div>
<!--
            <div class="button"><img src="assets/img/library.jpg"></div>
            <div class="modal">
                <div class="front"><img src="assets/img/library.jpg" alt="matryoshka"></div>
                <div class="back">
                    <div class="content">
                        <h1>Lorem ipsum</h1>
                        <p>laoreet.</p>
                    </div>
                </div>
                <div class="opened">
                    <div class="content">
                        <h1>Lorem ipsum</h1>
                        <p>laoreet. </p>
                    </div>
                    <div class="close"></div>
                </div>
            </div>
            <div class="wrapper"></div>
-->

            <div class="x_content">
                <!-- Tampilkan semua produk -->
                <div class="row">
                    <!-- looping products -->
                    <?php foreach($data_buku as $buku) { ?>
                    <div class="col-sm-3 col-md-3">
                        <div class="thumbnail">
                            <img src="<?php echo base_url();?>assets/upload/<?=$buku->gambar; ?>">
                        
                        <div class="caption">
                            <form>
                                
                                    <?=anchor('peminjaman/tambah_pinjam/' . $buku->id_buku, '
     Booking' , [
         'class' => 'btn btn-info',
         'role' => 'button'
    ])?>
                                    <?=anchor('buku/katalog_detail/' . $buku->id_buku, ' Detail' , [
         'class' => 'btn btn-warning glyphicon glyphicon-zoom-in',
         'role' => 'button'
    ])?>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- end looping -->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.alert alert-danger').alert().delay(3000).slideUp('slow');

    </script>
</body>

</html>
