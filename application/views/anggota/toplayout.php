<link rel="stylesheet" href="<?php echo base_url()?>assets/fontawesome/css/all.min.css">
<style type="text/css">
    .navbar-brand img {
        top: -25px;
        display: block;
        width: 58px;
        position: relative;

    }

    .navbar-default .navbar-collapse {
        border-color: #e7e7e7;
        padding: 11px;

    }

    .navbar-brand a {
        float: left;
        height: 30px;
        padding: 15px 15px;
        font-size: 28px;
        line-height: 10px;
        position: relative;
        left: 40px;
    }
    .navbar-default .navbar-brand{
        color: black;
    }

    .navbar-default .navbar-nav>li>a {
        color: #777;
        left: -100px;
        font-size: 20px;
    }

    .nav-tabs {
        left: 100px;
        font-size: 22px;
    }

    .done {
        position: relative;
        left: -60px;
        top: 2px;
    }

    ul li i {
        font-size: 25px;
        left: -90px;
        top: 10px;
        position: relative;
        padding: 0px 15px;
    }
    
    a{
        color: black;
    }
    a:hover{
        text-decoration: none;
        color: mediumblue;
    }

</style>


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-brand">
            <a class="navbar-brand" href="<?php echo base_url(); ?>Welcome"><img src="<?php echo base_url(); ?>assets/img/logo-bsiedit.png"></a>
            <?=anchor('member', 'Perpustakaan', ['class'=>'navbar-brand'])?>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <i class="fas fa-home">
                        <?php echo anchor('member', 'Home');?></i>
                </li>
                <li>
                    <i class="fas">
                        <?php
    $text_cart_url = '<span class="glyphicon glyphicon-shopping-cart" ariahidden="true"></span>';
    $text_cart_url .= ' Booking Cart: '. $this->m_perpus->edit_data(array('id_anggota'=>$this->session->userdata('id_agt')),'transaksi')->num_rows() .'Buku';
 ?>
                        <?=anchor('peminjaman/lihat_keranjang', $text_cart_url)?></i>
                </li>
                <?php if($this->session->userdata('id_agt')) { ?>
                <li class="nav-tabs">
                    <div style="line-height:50px;">Hi<b>
                            <?=$this->session->userdata('nama_agt')?></b></div>
                </li>
                <li class="done">
                    <i class="fas fa-chevron-circle-left">
                        <?php echo anchor('admin/logout', 'Logout');?></i>
                </li>
                <?php } else { ?>
                <li>
                    <?php echo anchor('welcome', 'Login');?>
                </li>
                <?php } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
