<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login - Perpustakaan Online</title>
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/styles.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url(). 'assets/fontawesome/css/all.min.css'; ?>">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'; ?>"></script>
</head>

<body>
    <script type="text/javascript">
        $('.alert-message').alert().delay(3000).slideUp('slow');

    </script>


    <div class="wrapper">

        <div class="login-form">

            <div class="title-section">
                <h1>login here!</h1>
            </div>
            <div class="login-section">
                <?php
			if(isset($_GET['pesan'])){
				if($_GET['pesan'] == "gagal"){
					echo "<div class='alert alert-danger alert-danger'>";
					echo $this->session->flashdata('alert');
					echo "</div>";
				}else if($_GET['pesan'] == "logout"){
					if($this->session->flashdata())
					{
						echo "<div class='alert alert-danger alert-success'>";
						echo $this->session->flashdata('Anda Telah Logout');
						echo "</div>";
					}
					//echo "<div class='alert alert-success'>Anda telah logout.</div>";
				}else if($_GET['pesan'] == "belumlogin"){
					if($this->session->flashdata())
					{
						echo "<div class='alert alert-danger alert-primary'>";
						echo $this->session->flashdata('alert');
						echo "</div>";
					}
					//echo "<div class='alert alert-primary'>Silahkan login dulu.</div>";
				}
			}else{
				if($this->session->flashdata())
				{
					echo "<div class='alert alert-danger alert-message'>";
					echo $this->session->flashdata('alert');
					echo "</div>";
				}
			}
		?>
                <form method="post" action="<?php echo base_url().'welcome/login'; ?>">
                    <div class="input-fields">
                        <i class="fas fa-user"></i>
                        <input type="text" name="admin_username" placeholder="username" class="input">
                        <?php echo form_error('username'); ?>
                    </div>
                    <div class="input-fields">
                        <i class="fas fa-unlock-alt"></i>
                        <input type="password" name="admin_password" placeholder="password" class="input">
                        <?php echo form_error('password'); ?>
                        <a href="#">forgot?</a>
                    </div>

                    <div>
                    <input type="submit" value="Login" class="btn">
                    </div>
                </form>

            </div>
        </div>
        <div class="box">
            <div class="icon"><i class="fas fa-book"></i></div>
            <div class="content">
                <h3>Motivation</h3>
                <p>“Manisnya akhirat mustahil diraih oleh orang-orang yang lebih suka terkenal di mata manusia”</p>
            </div>
        </div>
    </div>

</body>

</html>
