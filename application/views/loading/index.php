<html>
    <head>
        <title>Loding</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/loding/style.css">
    </head>
    <body onload="myfunction()">
    <div class="container" id="loader">
    <h3>Processing, please wait.</h3>
        <div class="progress-bar">
        <div class="shadow"></div>
        </div>    
    </div>
    <script src="<?php echo base_url();?>assets/loding/script.js">
       
        </script>    
    </body>
</html>