<html>

<head>
    <meta charset="utf-8">
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'; ?>"></script>
    <style type="text/css">
        .navbar {
            padding: 13px;
            height: 80px;
            background: rgba(0, 0, 0, 0.5);
        }

        .navbar-nav li {
            padding-right: 15px;
            text-align: center;
            font-size: 20px;
            padding-top: 15px;
            
        }

        .nav-link {
            font-size: 10px !important;
            font-family: times new roman;
            color: wheat;
            padding-left: 30px;
        }

        .nav-link:hover {
            text-decoration: none;
            color: red;
        }

        .container {
            top: -25px;
            position: relative;
        }


        h3 {
            color: wheat;
        }
        

    </style>
</head>
<nav class="navbar navbar-expand-sm">
    <div class="container">

        <div class="navbar-header">
            <?=anchor('member', 'Perpustakaan', ['class'=>'navbar-brand'])?>
        </div>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li>
                    <?php echo anchor('member', 'Home');?>
                </li>
                <li>
                    <?php
 $text_cart_url = '<span class="glyphicon glyphicon-shopping-cart" ariahidden="true"></span>';
 $text_cart_url .= ' Booking Cart: '. $this->m_perpus->edit_data(array('id_anggota'=>$this->session->userdata('id_agt')),'transaksi')->num_rows() .' Buku';
 ?>
                    <?=anchor('peminjaman/lihat_keranjang', $text_cart_url)?>
                </li>
                <?php if($this->session->userdata('id_agt')) { ?>
                <li>
                    <div style="line-height:50px;">Hai <b>
                            <?=$this->session->userdata('nama_agt')?></b></div>
                </li>
                <li>
                    <?php echo anchor('admin/logout', 'Logout');?>
                </li>
                <?php } else { ?>
                <li>
                    <?php echo anchor('welcome', 'Login');?>
                </li>
                <?php } ?>
            </ul>

        </div>
    </div>
    <!-- /.navbar-collapse -->
    <!-- /.container-fluid -->
</nav>
