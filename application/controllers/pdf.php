<?php
defined('BASEPATH') or exit ('NO Direct Script Access Allowed');


class Admin extends CI_Controller{
	function __construct(){
		parent::__construct();
		// cek login
		if($this->session->userdata('status') != "login"){
			$alert=$this->session->set_flashdata('alert', 'Anda belum Login');
			redirect(base_url());
		}
	}

	function index(){
		$data['peminjaman'] = $this->db->query("select * from transaksi order by id_pinjam desc limit 10")->result();
		$data['anggota'] = $this->db->query("select * from anggota order by id_anggota desc limit 10")->result();
		$data['buku'] = $this->db->query("select * from buku order by id_buku desc limit 10")->result();

		
		$this->load->view('tmplate/home', $data);
        $this->load->view('tmplate/footer');
		
	}
       function laporan_pdf_buku(){
        $this->load->library('dompdf_gen');
        
        $data['buku'] = $this->m_perpus->get_data('buku')->result();
        
        $this->load->view('admin/laporan_pdf_buku', $data);
        
        $paper_size = 'A4'; //ukuran kertas
        $orientation = 'landscape';
        $html = $this->output->get_output();
        
        $this->dompdf->set_paper($paper_size, $orientation);
        //convert to pdf
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("laporan_data_buku.pdf", array('Attachment'=>0));
        //nama file yang dihasilkan
    }